package com.eliasschwarze;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        Rectangle2D screenSize = Screen.getPrimary().getVisualBounds();
        scene = new Scene(loadFXML("MainView"), screenSize.getWidth() - 200, screenSize.getHeight() - 200);
        stage.setScene(scene);
        stage.show();
        stage.setMaximized(true);
        stage.setTitle("Texture Switcher");

        stage.setMinHeight(500);
        stage.setMinWidth(500);

        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);

        globalHotkeys(stage);
    }

    static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }






    private void globalHotkeys(Stage stage){
        scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
            public void handle(KeyEvent ke) {
                //Fullscreen mode
                if (ke.getCode() == KeyCode.F11) {

                    if(!stage.isFullScreen())
                    stage.setFullScreen(true);

                    else stage.setFullScreen(false);

                    ke.consume(); // <-- stops passing the event to next node
                }
            }
        });
    }

}