module com.eliasschwarze {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.eliasschwarze to javafx.fxml;
    exports com.eliasschwarze;
}